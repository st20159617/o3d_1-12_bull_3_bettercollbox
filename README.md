# o3d_1-12_bull_3_bettercollbox

Extended 2nd base project for CMU students studying Game Engine Development.

This one combines Ogre3d (1.12) and Bullet (3.0) the early examples I wrote used a slightly rubbish method of working out the collision box for ogre mesh objects (based on their AABBs from ogre). [BtOgre](https://github.com/OGRECave/btogre?files=1) provides a much more robust method, which I've used in this example, again btOgre is well worth looking into. 